# Problem1: Quadratic Equation Solver
# In problem1 you are to solve a quadratic equation in a form of ax^2 + bx + c = 0
#
# The roots of a quadratic equation can be calculated using the Quadratic Formula:
#   x = (−b ± sqrt(b^2 − 4ac)) / 2a
#
# Before finding the solution to the equation, your program should first output if the equation
# has a real solution. If a solution exists then you should display how many solutions there are
# in the equation.
#
# In order to check if a formula has a real solution, you need to calculate the discriminant of
# that formula using the equation below:
#   b^2 − 4ac
#
# The discriminant can be positive, zero, or negative, and this determines how many solutions
# there are to the given quadratic equation
# • A positive discriminant indicates that the quadratic has two distinct real number
# solutions.
# • A discriminant of zero indicates that the quadratic has a repeated real number solution.
# • A negative discriminant indicates that neither of the solutions are real numbers.
#
# The constant values (a, b, and c) are provided as the command line arguments. The output
# of this problem (for a = 1, b = 4, c = 3) should match the format provided below including
# white spaces and punctuations:
#
# There are two solutions
# Solution(s): -1.00 -3.00
#
# The order of command-line arguments:
# python 1.py a b c => python 1.py 1 4 3

import sys
import math


class QuadraticEquationSolver:

    @staticmethod
    def get_real_solutions(a, b, c) -> list:
        discriminant = QuadraticEquationSolver.get_discriminant(a, b, c)
        if discriminant > 0:
            return (-b + math.sqrt(discriminant)) / (2 * a), (-b - math.sqrt(discriminant)) / (2 * a)
        elif discriminant == 0:
            solution = []
            solution.append((-b) / (2 * a))
            return solution
        elif discriminant < 0:
            return []

    @staticmethod
    def get_discriminant(a, b, c) -> float:
        return b ** 2 - 4 * a * c


class ArgumentHandler:

    def __init__(self, arguments: list) -> None:
        self.__arguments = arguments
        try:
            self.__constants = self.__init_constants()
        except ArgumentNumberError as arg_err:
            exit(arg_err.message)
        except ValueError:
            exit("Arguments must be numberical.")

    def __init_constants(self) -> list:
        args = self.__arguments
        constants = []

        for arg_index in range(1, len(args)):
            constants.append(float(args[arg_index]))

        if len(constants) != 3:
            raise ArgumentNumberError("You must enter 3 parameters for a, b, and c respectively. You entered {}.".format(len(constants)))

        return constants

    def get_constants(self) -> list:
        return self.__constants


class Presenter:

    def __init__(self, solutions: list) -> None:
        self.__solutions = solutions

    def present(self) -> None:
        solutions = self.get_solutions()

        if len(solutions) == 2:
            print("There are two solutions.")
            print("Solutions: {0:.2f} {1:.2f}".format(round(solutions[0], 2), round(solutions[1], 2)))
        elif len(solutions) == 1:
            print("There is one solution.")
            print("Solution: {:.2f}".format(round(solutions[0])))
        elif len(solutions) < 1:
            print("There are no real solutions.")

    def get_solutions(self) -> list:
        return self.__solutions


class ArgumentNumberError(Exception):

    def __init__(self, message):
        self.message = message


def main(arguments: list) -> None:
    argument_handler = ArgumentHandler(arguments)
    constants = argument_handler.get_constants()
    solutions = QuadraticEquationSolver.get_real_solutions(constants[0], constants[1], constants[2])
    presenter = Presenter(solutions)
    presenter.present()


if __name__ == "__main__":
    main(sys.argv)
else:
    exit("You're trying to run this program by importing as a module.")
