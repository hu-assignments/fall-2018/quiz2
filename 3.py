# Problem3: Set Operations
# In this problem your program should find the intersection, union and difference of two sets
# (say A and B) each of which corresponds to different command line argument and the elements
# of A and B should be separated by commas. 
# 
# The output of problem3 should exactly match with the output format:
# 
# Set A: [’5’, ’14’, ’7’, ’9’, ’15’, ’42’]
# Set B: [’9’, ’4’, ’71’, ’5’]
# Intersection of A and B: [’5’, ’9’]
# Union of A and B: [’5’, ’14’, ’7’, ’9’, ’15’, ’42’, ’4’, ’71’]
# Difference of A and B: [’14’, ’7’, ’15’, ’42’]
# 
# NOTE: Do not use set collection of python.
# 
# The order of command-line arguments:
# python 3.py setA setB => python 3.py 5,14,7,9,15,42 9,4,71,5

import sys

class ArgumentHandler:
    
    def __init__(self, arguments: list) -> None:
        self.__arguments = arguments
        try:
            self.__sets = self.__init_sets()
        except ArgumentNumberError as arg_err:
            exit(arg_err.message)
        
    def __init_sets(self) -> list:
        args = self.__arguments
        
        if len(args) != 3:
            raise ArgumentNumberError("You must enter two arguments for the program. You entered {}.".format(len(args) - 1))
        
        set_A = self.convert_to_set(args[1].split(","))
        set_B = self.convert_to_set(args[2].split(","))

        return set_A, set_B

    def convert_to_set(self, candidate: list) -> list:
        converted = []
        for member in candidate:
            if member not in converted:
                converted.append(member)
        return converted

    def get_set_A(self) -> list:
        return self.__sets[0]

    def get_set_B(self) -> list:
        return self.__sets[1]


class SetOperator:

    @staticmethod
    def difference(set_A: list, set_B: list) -> list:
        difference = []
        for element in set_A:
            if element not in set_B:
                difference.append(element)
        return difference
    
    @staticmethod
    def intersection(set_A: list, set_B: list) -> list:
        intersection = []
        for element in set_A:
            if element in set_B:
                intersection.append(element)
        return intersection
    
    @staticmethod
    def union(set_A: list, set_B: list) -> list:
        union = []

        intersection = SetOperator.intersection(set_A, set_B)
        difference_A_B = SetOperator.difference(set_A, set_B)
        difference_B_A = SetOperator.difference(set_B, set_A)

        union.extend(intersection)
        union.extend(difference_A_B)
        union.extend(difference_B_A)

        return union


class ArgumentNumberError(Exception):

    def __init__(self, message):
        self.message = message


class Presenter:
    
    def __init__(self, set_A: list, set_B: list, intersection: list, union: list, difference: list) -> None:
        self.__set_A = set_A
        self.__set_B = set_B
        self.__intersection = intersection
        self.__union = union
        self.__difference = difference
    
    def present(self) -> None:
        print("Set A: {}".format(self.get_set_A()))
        print("Set B: {}".format(self.get_set_B()))
        print("Intersection of A and B: {}".format(self.get_intersection()))
        print("Union of A and B: {}".format(self.get_union()))
        print("Difference of A from B: {}".format(self.get_difference()))
    
    def get_set_A(self) -> list:
        return self.__set_A

    def get_set_B(self) -> list:
        return self.__set_B

    def get_intersection(self) -> list:
        return self.__intersection

    def get_union(self) -> list:
        return self.__union

    def get_difference(self) -> list:
        return self.__difference


def main(arguments: list) -> list:
    argument_handler = ArgumentHandler(arguments)

    set_A = argument_handler.get_set_A()
    set_B = argument_handler.get_set_B()
    intersection = SetOperator.intersection(set_A, set_B)
    union = SetOperator.union(set_A, set_B)
    difference = SetOperator.difference(set_A, set_B)

    presenter = Presenter(set_A, set_B, intersection, union, difference)

    presenter.present()





if __name__ == "__main__":
    main(sys.argv)
else:
    exit("You're trying to run this program by importing as a module.")