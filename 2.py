# Problem2: Even Number Evaluator
# In problem2, you are expected to consider only even numbers (E) within a list of numbers (L).
#
# The numbers in the list should be;
# a) greater than zero,
# b) provided in command line arguments and,
# c) separated by commas.
#
# Once you have obtained the even numbers
# from the list, you should print the following output considering the output format
# (for L = {75, 41, 14, 8, 73, 45, −16}):
#
# Even Numbers: [14, 8]
# Sum of Even Numbers: 22
# Even Number Rate: 0.086
#
# While calculating the even number rate, you should divide the sum of even numbers by the
# sum of all numbers in the list.
#
# NOTE: Your program should discard the numbers less than zero!
#
# The order of command-line arguments:
# python 2.py L => python 2.py 75,41,14,8,73,45,-16

import sys


class ArgumentHandler:

    def __init__(self, arguments: list) -> None:
        self.__arguments = arguments
        try:
            self.__number_list = self.__init_number_list()
        except ArgumentNumberError as arg_err:
            exit(arg_err.message)
        except ValueError:
            exit("Arguments must be numberical.")

    def __init_number_list(self) -> list:
        args = self.__arguments
        number_list = []

        if len(args) != 2:
            raise ArgumentNumberError("You must provide one argument for the program. You entered {}.".format(len(args) - 1))

        number_list = args[1].split(",")

        for i in range(len(number_list)):
            number_list[i] = float(number_list[i])

        return number_list

    def get_number_list(self) -> list:
        return self.__number_list


class ArgumentNumberError(Exception):

    def __init__(self, message):
        self.message = message


class Calculator:

    def __init__(self, number_list: list) -> None:
        self.__number_list = number_list

    def get_number_list(self) -> list:
        return self.__number_list

    def get_even_number_list(self) -> list:
        even_number_list = []
        number_list = self.get_number_list()
        for number in number_list:
            if number % 2 == 0 and number > 0:
                even_number_list.append(int(number))
        return even_number_list

    def get_even_number_rate(self) -> float:
        sum_of_even = 0
        sum_of_all_excluding_negative = 0
        number_list = self.get_number_list()
        even_number_list = self.get_even_number_list()
        for even in even_number_list:
            sum_of_even += even
        for number in number_list:
            if number >= 0:
                sum_of_all_excluding_negative += number
        return sum_of_even / sum_of_all_excluding_negative


class Presenter:

    def __init__(self, even_number_list: list, even_number_rate: float) -> None:
        self.__even_number_list = even_number_list
        self.__even_number_rate = even_number_rate

    def present(self) -> None:
        if len(self.__even_number_list) > 0:
            print("Even Numbers: {}".format(self.__even_number_list))
            print("Sum of Even Numbers: {}".format(int(sum(self.__even_number_list))))
        else:
            print("There are no even numbers in the list provided.")

        print("Even Number Rate: {}".format(round(self.__even_number_rate, 3)))


def main(arguments: list) -> None:
    argument_handler = ArgumentHandler(arguments)
    number_list = argument_handler.get_number_list()
    calculator = Calculator(number_list)
    presenter = Presenter(calculator.get_even_number_list(), calculator.get_even_number_rate())
    presenter.present()


if __name__ == "__main__":
    main(sys.argv)
else:
    exit("You're trying to run this program by importing as a module.")
